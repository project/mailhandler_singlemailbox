<?php
/**
 * Implement hook_menu().
 */
function mailhandler_singlebox_menu() {
  $items = array();
  $items['admin/config/content/singlebox'] = array(
    'title' => 'Mailhandler Single Mailbox',
    'description' => 'Administer settings for a single mailbox used for sending content to your site via email.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailhandler_singlebox_admin_form'),
    'access arguments' => array('configure singlebox'),
    'file' => 'mailhandler_singlebox.admin.inc',
  );
  $items['user/%/generate_email/%'] = array(
    'description' => 'AJAX callback for geneting new email addresses for mail media',
    'page callback' => 'mailhandler_singlebox_ajax',
    'page arguments' => array(1, 2, 3),
    'access callback' => TRUE,
  );

  $items['user/%/confirm_generate_email/%'] = $items['user/%/generate_email/%'];

  return $items;
}

/**
 * A tiny form for providing a 'confirm' button.
 */
function mailhandler_singlebox_modal_confirm_form($form, $form_state, $uid = ''){
  $form['#action'] = '/user/' . $uid . '/generate_email/ajax';
  $form['confirm'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#attributes' => array (
        'class' => array('use-ajax-submit')
    ),
  );
  return $form;
}

/**
 * Ajax callback for regenerating a mailhandler_singlebox email addresses
 */
function mailhandler_singlebox_ajax($uid, $action, $js){
  global $user;
  // We're using the ajax callback for two things.  One to prompt for confirmation
  // and the other is to actually generate the email address.  For confirmation
  // we will use a dialog if dialog module exists otherwise we'll fall back to
  // dsming a message to the screen.  If all else fails we just send the use
  // to the user/edit page for the regeneration of email.
  if ($js == 'ajax') {
    // If we don't have a user id, we're likely on a page like /user.
    // WSCCI can't come soon enough.
    if ($uid == '') {
      $uid = $user->uid;
      $acted_on_user = $user;
    }
    else {
      $acted_on_user = user_load($uid);
    }
    if ($action == 'confirm_generate_email'){
      $are_you_sure = t('Are you sure you want to regenerate this email address? This action cannot be undone.');
      // Check for dialog module
      if (module_exists('dialog')) {
        // Set some options for the modal dialog we want to show.
        $options = array('modal' => TRUE, 'resizable' => FALSE, 'dragable' => FALSE);
        dialog_display($options);
        // Get and render a tiny form with a confirm button.
        $form = drupal_get_form('mailhandler_singlebox_modal_confirm_form', $acted_on_user->uid);
        $form = drupal_render($form);
        // Prepare content for delivery to dialog.
        $output[] = dialog_command_display($are_you_sure . $form, array('title' => t('Are you sure?')));
        ajax_deliver(array('#type' => 'ajax', '#commands' => $output));
      }
      else {
        $ajax_renew_link = l(t('Continue?'), 'user/' . $account->uid . '/generate_email/ajax', array('attributes'=>array('class' => 'use-ajax')));
        drupal_set_message($are_you_sure . ' ' . $ajax_renew_link, 'warning');
        $commands[] = ajax_command_html('#generated-mail-wrapper', theme('status_messages'));
        return ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
      }
    }
    elseif($action == 'generate_email') {
      if (user_access('submit content by mail via singlebox', $user) && ($user->uid == $uid || user_access('administer users', $user))){
        // We generate the new address with the user name as a base.
        $generated_mail = mailhandler_singlebox_generate_email($acted_on_user->name);
        if ($generated_mail && mailhandler_singlebox_save_generated_address($uid, $generated_mail)){
          drupal_set_message(t('The address has been regenerated'));
          $commands[] = ajax_command_html('#generated-mail-wrapper', theme('status_messages') . $generated_mail);
          if (module_exists('dialog')) {
            $commands[] = dialog_command_dismiss();
          }
          return ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
        }
        else {
          drupal_set_message(t('Address could not be regenerated'), 'error');
          $commands[] = ajax_command_before('#generated-mail-wrapper',theme('status_messages'));
          if (module_exists('dialog')) {
            $commands[] = dialog_command_dismiss();
          }
          return ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
        }
      }
      else {
        // This case should never happen via UI, but it could happen via direct call.
        drupal_set_message(t('Permission denied.'), 'error');
        $commands[] = ajax_command_before('#generated-mail-wrapper',theme('status_messages'));
        return ajax_deliver(array('#type' => 'ajax', '#commands' => $commands));
      }
    }
  }
  else {
    drupal_goto('/user/' . $uid . '/edit');
  }
}

function mailhandler_singlebox_form_user_admin_permissions_alter(&$form, &$form_state, $form_id) {
  // Add a way to garbage collect when permissions are saved.
  $form['#submit'][] = 'mailhandler_singlebox_garbage_collection';
}

/**
 * Helper function to clear out addresses that no longer have permissions.
 * This should not be called directly. It gets called with the saving of permissions.
 */
function mailhandler_singlebox_garbage_collection() {
  $addresses = mailhandler_singlebox_get_valid_addresses();
  $user = new stdClass();
  foreach ($addresses as $address => $uid) {
    // Build little fake user with a uid so we can check his permissions.
    $user->uid = $uid;
    //see if each user still has permission to send media by mail
    if (!user_access('submit content by mail via singlebox', $user)) {
      db_delete('mailhandler_singlebox_addresses')
        ->condition('uid', $uid)
        ->execute();
    }
  }
  // Invalidate our own cache.  Permission saving does clear all caches just
  // before this function but our get address function rebuilt ours.
  cache_clear_all('mailhandler_singlebox_valid_addresses', 'cache');
}

// Function to generate some UI for regenerating addresses for either the user
// view or edit page.  Kind of strange to do it this way. The module may be changed
// in the future, but for now this meets requirements.
function mailhandler_singlebox_regenerate_ui($actedon_user) {
  global $user;
  // Check that the user has permission to generate a new address for themselves
  // or for the user they are acting on.
  if (user_access('submit content by mail via singlebox', $actedon_user) && (($user->uid == $actedon_user->uid) || user_access('administer users', $user))) {
    // Ensure that the site is configured to allow email address generation.
    if (!variable_get('mailhandler_singlebox_default_mailbox_name', NULL)) {
<<<<<<< HEAD
      drupal_set_message(t('A default mailbox has not been defined for emailing content. See !adminlink prior to generating a new address', array('!adminlink' => l(t('the configuration page'), 'admin/config/content/singlebox', array('query' => array('destination' => 'user/' . arg(1) . '/edit'))))), 'warning');
=======
      drupal_set_message(t('A default mailbox has not been defined for emailing content. See !adminlink prior to generating a new address', array('!adminlink' => l(t('the configuration page'), 'admin/config/media/singlebox', array('query' => array('destination' => 'user/' . arg(1) . '/edit'))))), 'warning');
>>>>>>> ba1f2679d20468d608304b3d479c9f64c24e9f5b
    }
    else {
      drupal_add_js('misc/ajax.js');
      // If dialog module exists we want to load the libarary
      // for a confirmation message on both the view and edit pages.
      if (module_exists('dialog')) {
        // Load the dialog library.
        drupal_add_library('dialog', 'dialog');
      }
      $ajax_renew_link = l(t('Generate new address'), 'user/'. $actedon_user->uid . '/confirm_generate_email/nojs', array('attributes'=>array('class' => 'use-ajax')));
      // Get the currently configured email address for this user.
      $addresses = mailhandler_singlebox_get_valid_addresses();
      if ($key = array_search($actedon_user->uid, $addresses)) {
        $generated_mail = $key;
      }
      else {
        // Show a default that isn't actually stored or valid, but provides feedback to the user.
        $generated_mail = 'email@' . mailhandler_singlebox_get_default_mailbox_domain();
      }
      $markup = $ajax_renew_link . '<div id="generated-mail-wrapper">' . $generated_mail . '</div>';
    }
  }
  //generate the right markup
  return $markup;
}


/**
 *  Implements hook_form_FORM_ID_alter().
 */
function mailhandler_singlebox_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  if ($markup = mailhandler_singlebox_regenerate_ui($form['#user'])) {
    $form['mailhandler_singlebox'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE, //@TODO change to true when done debug
      '#title' => t('Generated email address'),
      '#description' => t('Generated email addresses are used for validating content submitted via email. Content can only be sent to these generated addresses for inclusion on the site.'),
    );
    $form['mailhandler_singlebox']['regenerate'] = array(
      '#type' => 'markup',
      '#markup' =>  $markup,
    );
  }
}


/**
 * Implements hook_user_view().
 */
function mailhandler_singlebox_user_view($account, $view_mode, $langcode) {
  if ($markup = mailhandler_singlebox_regenerate_ui($account)) {
    $account->content['foo']['bar'] =  array(
      '#type' => 'user_profile_item',
      '#title' => t('Email address for submitting content.') . ' ' . $ajax_renew_link,
      '#markup' => $markup,
      '#attributes' => array('class' => array('mail-media')),
    );
  }
}

/**
 * Submit handler for mailhandler_singlebox alteration to the user profile form
 * Saves the generated email address for the user.  These addresses can be
 * used as a TO validator in mailhandler.
 */
function mailhandler_singlebox_profile_form_submit(&$form, &$form_state) {
  $address = $form_state['values']['generated_mail'];
  mailhandler_singlebox_save_generated_address($form['#user']->uid, $address);
}

/**
 * Function to actually store mailhandler_singlebox addresses in the database
 * Called from altered profile form submit and possibly an ajax callback
 */
function mailhandler_singlebox_save_generated_address($uid, $address) {
  if ($address != 'email@' . mailhandler_singlebox_get_default_mailbox_domain()) {
    db_delete('mailhandler_singlebox_addresses')
      ->condition('uid', $uid)
      ->execute();

    $fields = array('uid' => $uid, 'mail' => strtolower($address));
    db_insert('mailhandler_singlebox_addresses')
      ->fields($fields)
      ->execute();

    // Invalidate cache of valid addresses.
    cache_clear_all('mailhandler_singlebox_valid_addresses', 'cache');
    return TRUE;
  }
  else {
    return FALSE;
  }
}

///**
// @TODO - I'm 99% sure I don't need this now.
// * A field showing the generated email address to the user and letting them
// * know that they must still save their user profile in order for the change
// * to be saved.
// */
//function mailhandler_singlebox_generate_email_field(&$form, &$form_state) {
//  $generated_mail = mailhandler_singlebox_generate_email($form['#user']->name);
//  $form['mailhandler_singlebox']['generated_mail']['#default_value'] = $generated_mail;
//  $form['mailhandler_singlebox']['generated_mail']['#value'] = $generated_mail;
//  drupal_set_message(t('A new address has been generated. You must save your profile in order to keep these changes'), 'warning');
//  $markup = theme('status_messages');
//  $markup .= '<span class="updated">' . check_plain($generated_mail) . '</span>';
//  $form['mailhandler_singlebox']['generated_mail_text']['#markup'] = $markup;
//  return $form['mailhandler_singlebox'];
//}

/**
 * Helper/Wrapper function to generate a new mailhandler_singlebox address.
 * The work of actually generating addresses is handed off to a ctools plugin.
 */
function mailhandler_singlebox_generate_email($base) {
  // Load the plugin responsible for generating email addresses.
  // This module provides two plugins by default (for now) See plugins directory:
  //   1) A catch all mailbox is used and we create an address in the format username-RANDOMNESS@example.com
  //   2) A plus addressing scheme is used and we create an address in the format baseaddress+username-RANDOMNESS@example.com
  $plugin_id = variable_get('mailhandler_singlebox_addressing_scheme', '');
  ctools_include('plugins');
  if ($class = ctools_plugin_load_class('mailhandler_singlebox', 'mailhandler_singlebox_address_generator', $plugin_id, 'handler')){
    $generator = new $class();
    $new_address = $generator->generate($base);
    return $new_address;
  }
  else {
<<<<<<< HEAD
    drupal_set_message(t('An address could not be generated. Ensure that a address generator has been specified in the site configuration. See !adminlink prior to generating a new address', array('!adminlink' => l(t('the configuration page'), 'admin/config/content/singlebox', array('query' => array('destination' => 'user/' . arg(1) . '/edit'))))), 'error');
=======
    drupal_set_message(t('An address could not be generated. Ensure that a address generator has been specified in the site configuration. See !adminlink prior to generating a new address', array('!adminlink' => l(t('the configuration page'), 'admin/config/media/singlebox', array('query' => array('destination' => 'user/' . arg(1) . '/edit'))))), 'error');
>>>>>>> ba1f2679d20468d608304b3d479c9f64c24e9f5b
    return FALSE;
  }
}

/**
 * Helper function to return the domain portion of the default mailbox.
 */
function mailhandler_singlebox_get_default_mailbox_domain() {
  // Get the name of the mailhandler mailbox that has been set to be the default for this site.
  // Use that to determine the domain for generated account names
  static $domain;
  if ($domain) {
    return $domain;
  }
  else {
    if ($default_mailbox_name = variable_get('mailhandler_singlebox_default_mailbox_name', NULL)) {
      $mailbox =  mailhandler_mailbox_load($default_mailbox_name);
      $domain = substr($mailbox->settings['name'], strpos($mailbox->settings['name'], '@')+1);
    }
    else {
      $domain = 'example.com';
    }
    return $domain;
  }
}

/**
 * A function to return all valid email addresses that can be used to send media to.
 * Email address is forced to lower case for comparison.
 *
 * @return An array of email addresses => uid mappings
 */
function mailhandler_singlebox_get_valid_addresses() {
  $addresses = &drupal_static(__FUNCTION__);
  if (is_array($addresses)) {
    return $addresses;
  }
  else {
    $address_cache = cache_get('mailhandler_singlebox_valid_addresses');
    if (is_object($address_cache)) {
      $addresses = $address_cache->data;
      return $addresses;
    }
    else {
      // Fetch a keyed array uid => mailhandler_singlebox_address
      $addresses = db_select('mailhandler_singlebox_addresses')->fields('mailhandler_singlebox_addresses')->execute()->fetchAllKeyed();
      // Flip the array.  We're going to be looking up the email address far more often than the uid.
      $addresses = array_flip($addresses);
      // Cache the results.  Cache is explicity expired if new addresses are added or updated.
      cache_set('mailhandler_singlebox_valid_addresses', $addresses);
      return $addresses;
    }
  }
}

/**
 * Implements hook_mailhandler_sendto_addresses().
 *
 * @return array of valid addresses.
 */
function mailhandler_singlebox_mailhandler_sendto_addresses() {
  return mailhandler_singlebox_get_valid_addresses();
}

/**
 * Implements hook_permission().
 */
function mailhandler_singlebox_permission() {
  return array(
    'submit content by mail via singlebox' => array(
      'title' => t('Submit content by mail via mailhandler singlebox'),
      'description' => t('A user with this permission will be able to generate an email address to which they can content for publication on this site. Those messages may contain media attachments (photos or video URLs)'),
    ),
    'configure singlebox' => array(
      'title' => t('Configure mailhandler singlebox settings'),
      'description' => t('A user with this permission can configure mailhandler singlebox settings such as the mailbox to use and the format of addresses generated for users with the "submit content by mail via singlebox" permission.'),
    ),
  );
}


/**
 * Implements hook_ctools_plugin_type().
 */
function mailhandler_singlebox_ctools_plugin_type() {
  return array(
    'mailhandler_singlebox_address_generator' => array(
      'use hooks' => FALSE,
    ),
  );
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function mailhandler_singlebox_ctools_plugin_directory($module, $plugin){
  if ($module == 'mailhandler_singlebox' && $plugin == 'mailhandler_singlebox_address_generator') {
    return 'plugins/address_generator';
  }
}